<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use SoapClient;

class SoapWsdlTest extends TestCase
{

	private $params = [
		'soap_version' => SOAP_1_2,
		'trace' => true
	];

	public function testSobeTons()
	{

		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->riseTones('C', 5.5);

		$this->assertEquals('B', $response);
	}

	public function testSobeUmaOitava()
	{

		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->riseTones('D', 6);

		$this->assertEquals('D', $response);
	}

	public function testSobeTonsComRepeticaoDeEscala()
	{

		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->riseTones('B', 0.5);

		$this->assertEquals('C', $response);
	}

	public function testSobeTonsComNotaMusicalInvalida()
	{
		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->riseTones('Z', 1);

		$this->assertEquals('Invalid musical note', $response);
	}

	public function testSobeTonsComTonsInvalidos()
	{

		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->riseTones('C', 1.1);

		$this->assertEquals('Invalid number of tones', $response);
	}

	public function testDesceTons()
	{

		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->fallTones('G', 2.5);

		$this->assertEquals('D', $response);
	}


	public function testDesceUmaOitava()
	{

		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->fallTones('E', 6);

		$this->assertEquals('E', $response);
	}

	public function testDesceTonsComRepeticaoDeEscala()
	{
		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->fallTones('C', 1);

		$this->assertEquals('A#', $response);
	}

	public function testDesceTonsComNotaMusicalInvalida()
	{

		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->fallTones('', 1);

		$this->assertEquals('Invalid musical note', $response);
	}

	public function testDesceTonsComTonsInvalidos()
	{

		$client = new SoapClient('http://localhost:8081/app/index.wsdl', $this->params);
		$response = $client->fallTones('C', 3.2);

		$this->assertEquals('Invalid number of tones', $response);
	}
}
