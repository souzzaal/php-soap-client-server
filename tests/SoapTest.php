<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\MySoapClient;
use SoapClient;

class SoapTest extends TestCase
{
	public function testSobeTons()
	{
		$client = new MySoapClient();
		$response = $client->sobeTons('C', 5.5);

		$this->assertEquals('B', $response);
	}

	public function testSobeUmaOitava()
	{
		$client = new MySoapClient();
		$response = $client->sobeTons('D', 6);

		$this->assertEquals('D', $response);
	}

	public function testSobeTonsComRepeticaoDeEscala()
	{
		$client = new MySoapClient();
		$response = $client->sobeTons('B', 0.5);

		$this->assertEquals('C', $response);
	}

	public function testSobeTonsComNotaMusicalInvalida()
	{
		$client = new MySoapClient();
		$response = $client->sobeTons('Z', 1);

		$this->assertEquals('Invalid musical note', $response);
	}

	public function testSobeTonsComTonsInvalidos()
	{
		$client = new MySoapClient();
		$response = $client->sobeTons('C', 1.1);

		$this->assertEquals('Invalid number of tones', $response);
	}

	public function testDesceTons()
	{
		$client = new MySoapClient();
		$response = $client->desceTons('G', 2.5);

		$this->assertEquals('D', $response);
	}


	public function testDesceUmaOitava()
	{
		$client = new MySoapClient();
		$response = $client->desceTons('E', 6);

		$this->assertEquals('E', $response);
	}

	public function testDesceTonsComRepeticaoDeEscala()
	{
		$client = new MySoapClient();
		$response = $client->desceTons('C', 1);

		$this->assertEquals('A#', $response);
	}

	public function testDesceTonsComNotaMusicalInvalida()
	{
		$client = new MySoapClient();
		$response = $client->desceTons('', 1);

		$this->assertEquals('Invalid musical note', $response);
	}

	public function testDesceTonsComTonsInvalidos()
	{
		$client = new MySoapClient();
		$response = $client->desceTons('C', 3.2);

		$this->assertEquals('Invalid number of tones', $response);
	}
}
