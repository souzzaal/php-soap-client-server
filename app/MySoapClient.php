<?php

namespace App;

use SoapClient;

class MySoapClient
{
	private $client;
	public function __construct()
	{
		$params = [
			'location' => 'http://localhost:8080/app/MySoapServer.php',
			'uri' => 'http://localhost:8080/app/MySoapServer.php',
		];

		$this->client = new SoapClient(NULL, $params);
	}

	public function sobeTons(string $nota, float $ntons)
	{
		return $this->client->__soapCall('riseTones', [$nota, $ntons]);
	}

	public function desceTons(string $nota, float $ntons)
	{
		return $this->client->__soapCall('fallTones', [$nota, $ntons]);
	}
}
