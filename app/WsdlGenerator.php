<?php

namespace App;

require_once __DIR__ . '/../vendor/autoload.php';

use PHP2WSDL\PHPClass2WSDL;

class WsdlGenerator
{
	public function __construct()
	{
		$class = new MySoapServer;
		$serviceURI = "http://localhost:8081/app/index.wsdl";

		$wsdlGenerator = new PHPClass2WSDL($class, $serviceURI);
		// Generate the WSDL from the class adding only the public methods that have @soap annotation.
		$wsdlGenerator->generateWSDL(true);
		// Dump as string
		// $wsdlXML = $wsdlGenerator->dump();
		// Or save as file
		$wsdlXML = $wsdlGenerator->save('app/index.wsdl');
	}
}

new WsdlGenerator();
