<?php

namespace App\Services;

trait TranspositionService
{

	private $musicalNotes = [
		'C',
		'C#',
		'D',
		'D#',
		'E',
		'F',
		'F#',
		'G',
		'G#',
		'A',
		'A#',
		'B'
	];

	/**
	 * @soap
	 * 
	 * @param string $note
	 * @param float $numberOfTones
	 * @return string $noteTransposta
	 */
	public function riseTones(string $note, float $numberOfTones)
	{
		$chave = array_search($note, $this->musicalNotes);

		if ($chave === false) {
			return 'Invalid musical note';
		}

		if (fmod(floatval($numberOfTones), 0.5) > 0) {
			return 'Invalid number of tones';
		}

		$semitones = $numberOfTones / 0.5;

		while ($semitones >= 12) {
			$semitones -= 12;
		}

		if ($semitones == 0) {
			return $note;
		} else {

			for ($i = 1; $i <= $semitones; $i++) {
				if (($chave + $i) > count($this->musicalNotes) - 1) {
					$chave = -1;
				}
				$noteTransposta = $this->musicalNotes[$chave + $i];
			}
			return $noteTransposta;
		}
	}

	/**
	 * @soap
	 * 
	 * @param string $note
	 * @param float $numberOfTones
	 * @return string $noteTransposta
	 */
	public function fallTones(string $note, float $numberOfTones)
	{
		$chave = array_search($note, $this->musicalNotes);

		if ($chave === false) {
			return 'Invalid musical note';
		}

		if (fmod(floatval($numberOfTones), 0.5) > 0) {
			return 'Invalid number of tones';
		}

		$semitones = $numberOfTones / 0.5;

		while ($semitones >= 12) {
			$semitones -= 12;
		}

		if ($semitones == 0) {
			return $note;
		} else {

			for ($i = 1; $i <= $semitones; $i++) {

				if (($chave - $i) < 0) {
					$chave = count($this->musicalNotes);
				}

				$noteTransposta = $this->musicalNotes[$chave - $i];
			}
			return $noteTransposta;
		}
	}
}
