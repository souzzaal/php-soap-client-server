<?php

namespace App;

require __DIR__ . "/../vendor/autoload.php";

use App\Services\TranspositionService;
use SoapServer;

class MySoapServer
{
	use TranspositionService;
}

$params = [
	'uri' => 'http://localhost:8080/app/MySoapServer.php'
];

$server = new SoapServer(NULL, $params);
$server->setObject(new MySoapServer());
$server->handle();
