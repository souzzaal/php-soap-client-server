<?php

namespace App;

require __DIR__ . "/../vendor/autoload.php";

use App\Services\TranspositionService;
use SoapServer;

class MySoapWsdlServer
{
	use TranspositionService;
}

$params = [
	'soap_version' => SOAP_1_2
];

$server = new SoapServer('http://localhost:8081/index.wsdl', $params);
$server->setObject(new MySoapWsdlServer());
$server->handle();
