# Cliente e servidor SOAP com PHP

Pequeno projeto visando estudar e demonstrar a disponibilização e consumo de informações usando SOAP (com e sem arquivos WSDL) + PHP.

O plano de fundo o projeto é a oferta de um serviço transposição notas (não acordes) musicais em função de tons e semitons.

## Tecnologias utilizadas

- PHP
  - Precisa da biblioteca php-soap instalada
- Composer
- PhpUnit
- PHP2WSDL\PHPClass2WSDL

## Testando SOAP sem WSDL

Para essa abordagem, foi utilizada a classe `MySoapClient` como  uma interface de comunição entre a aplicação e o servidor.

1. Baixar e instalar as dependências do projeto
2. Acessar o diretório do  projeto e executar `composer run-script --timeout=0 start-server`
3. Acessar o diretório do projeto e executar `composer test tests/SoapTest.php`

## Testando SOAP com WSDL

Já nesta abordagem, a aplicação se comunica direatamente com o servidor.

1. Baixar e instalar as dependências do projeto
2. Acessar o diretório do  projeto e executar `composer run-script --timeout=0 start-wsdl-server`
3. Acessar o diretório do projeto e executar `composer test tests/SoapWsdlTest.php`

## Extras

### Gerando o arquivo WSDL

O arquivo WSDL foi gerado a partir da biblioteca PHP2WSDL\PHPClass2WSD, que se utiliza das anotações na documentação dos métodos de uma classe PHP.

O script de geração do arquivo arquivo WSDL pode executado através seguinte comando: `composer generate-wsdl`.

Após a criação do arquivo, deve-se alterar o `location` da tag `soap:address` afim de informar a URL do arquivo PHP em que o servidor SOAP foi disponibilizado.

```xml
 <service name="App.MySoapServerService">
    <port name="App.MySoapServerPort" binding="tns:App.MySoapServerBinding">
      <soap:address location="http://localhost:8081/app/MySoapWsdlServer.php"/>
    </port>
  </service>
```

### Configurarndo o PhpUnit

1. Crie o projeto com `composer init`
2. Adicione o PhpUnit com `composer require phpunit/phpunit`
3. Na raiz do projeto crie os diretórios `app` (para o código-fonte, por padrão o assitente sugere `src`) e `tests` (para os arquivos de teste)
4. Execute `./vendor/bin/phpunit --generate-configuration` e responda as perguntas do assitente.

## Referências

[PHPSP](https://phpsp.org.br/artigos/php-soap/ "PHP + SOAP")